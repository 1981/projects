<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String contextPath = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>饼柱混搭</title>
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/js/easyui/themes/icon.css">
<script type="text/javascript" src="<%=contextPath %>/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="<%=contextPath %>/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=contextPath %>/js/echarts/echarts-all.js"></script>
</head>
<body>
	
	<div style="height:400px; width: 800px; border: 1px solid #ccc; padding: 3px;">
		<div align="right" style="line-height:25px;vertical-align:middle; border: 1px solid #ccc;padding: 1px;">
		<input type="checkbox" name="checkAll" onclick="selectAll(this);" checked="checked">全选 &nbsp;&nbsp;
			渠道名称：
			<select class="easyui-select" id="" name="">
				<option>全部</option>
				<option value="11">11</option>
				<option value="22">22</option>
			</select>
			时间段
			<input type="text" class="easyui-text" name="beginDate"/>--<input type="text" class="easyui-text" name="endDate"/>
			<a href="#"><img src="<%=contextPath %>/images/excelicon.jpg" align="middle"></a>
		</div>
		<div id="main" style="height:90%; width: 100%;"></div>
	</div>
	<script type="text/javascript">
		// 基于准备好的dom，初始化echarts图表
		var myChart = echarts.init(document.getElementById('main'));

		var option = {
			    tooltip : {
			        trigger: 'axis'
			    },
			    toolbox: {
			        show : true,
			        y: 'top',
			        feature : {
	
			            magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    calculable : true,
			    legend: {
			        data:['直接访问','邮件营销','联盟广告','视频广告','搜索引擎','百度','谷歌','必应','其他']
			    },
			    xAxis : [
			        {
			            type : 'category',
			            splitLine : {show : false},
			            data : ['周一','周二','周三','周四','周五','周六','周日']
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value',
			            position: 'right'
			        }
			    ],
			    series : [
			        {
			            name:'直接访问',
			            type:'bar',
			            data:[320, 332, 301, 334, 390, 330, 320]
			        },
			        {
			            name:'邮件营销',
			            type:'bar',
			            tooltip : {trigger: 'item'},
			            stack: '广告',
			            data:[120, 132, 101, 134, 90, 230, 210]
			        },
			        {
			            name:'联盟广告',
			            type:'bar',
			            tooltip : {trigger: 'item'},
			            stack: '广告',
			            data:[220, 182, 191, 234, 290, 330, 310]
			        },
			        {
			            name:'视频广告',
			            type:'bar',
			            tooltip : {trigger: 'item'},
			            stack: '广告',
			            data:[150, 232, 201, 154, 190, 330, 410]
			        },
			        {
			            name:'搜索引擎',
			            type:'line',
			            data:[862, 1018, 964, 1026, 1679, 1600, 1570]
			        },

			        {
			            name:'搜索引擎细分',
			            type:'pie',
			            tooltip : {
			                trigger: 'item',
			                formatter: '{a} <br/>{b} : {c} ({d}%)'
			            },
			            center: [160,130],
			            radius : [0, 50],
			            itemStyle :{
			                normal:{
			                    labelLine : {
			                        length : 20
			                    }
			                }
			            },
			            data:[
			                {value:1048, name:'百度'},
			                {value:251, name:'谷歌'},
			                {value:147, name:'必应'},
			                {value:102, name:'其他'}
			            ]
			        }
			    ]
			};
		// 为echarts对象加载数据 
		myChart.setOption(option);
	
		function selectAll(checkbox){
			if(checkbox.checked){
				var myChart = echarts.init(document.getElementById('main'));			
				this.option.legend.selected = {'直接访问':true,'邮件营销':true,'联盟广告':true,'视频广告':true,'搜索引擎':true,'百度':true,'谷歌':true,'必应':true,'其他':true};
			}else{
				var myChart = echarts.init(document.getElementById('main'));
				option.legend.selected = {'直接访问':false,'邮件营销':false,'联盟广告':false,'视频广告':false,'搜索引擎':false,'百度':false,'谷歌':false,'必应':false,'其他':false};
			}
			myChart.setOption(option);
		};
	</script>
</body>
</html>