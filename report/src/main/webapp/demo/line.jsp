<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String contextPath = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>折线图</title>
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/js/easyui/themes/icon.css">
<script type="text/javascript" src="<%=contextPath %>/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="<%=contextPath %>/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=contextPath %>/js/echarts/echarts-all.js"></script>
</head>
<body>
	
	<div style="height:400px; width: 800px; border: 1px solid #ccc; padding: 3px;">
		<div align="right" style="line-height:25px;vertical-align:middle; border: 1px solid #ccc;padding: 1px;">
		<input type="checkbox" name="checkAll" onclick="selectAll(this);" checked="checked">全选 &nbsp;&nbsp;
			渠道名称：
			<select class="easyui-select" id="" name="">
				<option>全部</option>
				<option value="11">11</option>
				<option value="22">22</option>
			</select>
			时间段
			<input type="text" class="easyui-text" name="beginDate"/>--<input type="text" class="easyui-text" name="endDate"/>
			<a href="#"><img src="<%=contextPath %>/images/excelicon.jpg" align="middle"></a>
		</div>
		<div id="main" style="height:90%; width: 100%;"></div>
	</div>
	<script type="text/javascript">
		// 基于准备好的dom，初始化echarts图表
		var myChart = echarts.init(document.getElementById('main'));

		var option = {
			    title : {
			        text: '未来一周气温变化',
			        subtext: '纯属虚构'
			    },
			    tooltip : {
			        trigger: 'axis'
			    },
			    legend: {
			        data:['最高气温','最低气温']
			    },
			    toolbox: {
			        show : true,
			        feature : {
			            magicType : {show: true, type: ['line', 'bar']},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    calculable : true,
			    xAxis : [
			        {
			            type : 'category',
			            boundaryGap : false,
			            data : ['周一','周二','周三','周四','周五','周六','周日']
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value',
			            axisLabel : {
			                formatter: '{value} °C'
			            }
			        }
			    ],
			    series : [
			        {
			            name:'最高气温',
			            type:'line',
			            data:[11, 11, 15, 13, 12, 13, 10],
			            markPoint : {
			                data : [
			                    {type : 'max', name: '最大值'},
			                    {type : 'min', name: '最小值'}
			                ]
			            },
			            markLine : {
			                data : [
			                    {type : 'average', name: '平均值'}
			                ]
			            }
			        },
			        {
			            name:'最低气温',
			            type:'line',
			            data:[1, -2, 2, 5, 3, 2, 0],
			            markPoint : {
			                data : [
			                    {name : '周最低', value : -2, xAxis: 1, yAxis: -1.5}
			                ]
			            },
			            markLine : {
			                data : [
			                    {type : 'average', name : '平均值'}
			                ]
			            }
			        }
			    ]
			};
			                    

		// 为echarts对象加载数据 
		myChart.setOption(option);
		function selectAll(checkbox){
			if(checkbox.checked){
				var myChart = echarts.init(document.getElementById('main'));			
				this.option.legend.selected = {'最高气温':true,'最低气温':true};
			}else{
				var myChart = echarts.init(document.getElementById('main'));
				option.legend.selected = {'最高气温':false,'最低气温':false};
			}
			myChart.setOption(option);
		};

	</script>
</body>
</html>