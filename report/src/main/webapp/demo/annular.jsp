<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String contextPath = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>环形图</title>
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/js/easyui/themes/icon.css">
<script type="text/javascript" src="<%=contextPath %>/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="<%=contextPath %>/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=contextPath %>/js/echarts/echarts-all.js"></script>
</head>
<body>
	
	<div style="height:400px; width: 800px; border: 1px solid #ccc; padding: 3px;">
		<div align="right" style="line-height:25px;vertical-align:middle; border: 1px solid #ccc;padding: 1px;">
		<input type="checkbox" name="checkAll" onclick="selectAll(this);" checked="checked">全选 &nbsp;&nbsp;
			渠道名称：
			<select class="easyui-select" id="" name="">
				<option>全部</option>
				<option value="11">11</option>
				<option value="22">22</option>
			</select>
			时间段
			<input type="text" class="easyui-text" name="beginDate"/>--<input type="text" class="easyui-text" name="endDate"/>
			<a href="#"><img src="<%=contextPath %>/images/excelicon.jpg" align="middle"></a>
		</div>
		<div id="main" style="height:90%; width: 100%;"></div>
	</div>
	<script type="text/javascript">
		// 基于准备好的dom，初始化echarts图表
		var myChart = echarts.init(document.getElementById('main'));

		var option = {
			    tooltip : {
			        trigger: 'item',
			        formatter: "{a} <br/>{b} : {c} ({d}%)"
			    },
			    legend: {
			        orient : 'vertical',
			        x : 'left',
			        data:['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
			    },
			    toolbox: {
			        show : true,
			        feature : {
			            magicType : {
			                show: true, 
			                type: ['pie', 'funnel'],
			                option: {
			                    funnel: {
			                        x: '25%',
			                        width: '50%',
			                        funnelAlign: 'center',
			                        max: 1548
			                    }
			                }
			            },
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    calculable : true,
			    series : [
			        {
			            name:'访问来源',
			            type:'pie',
			            radius : ['40%', '70%'],
			            itemStyle : {
			                normal : {
			                    label : {
			                        show : false
			                    },
			                    labelLine : {
			                        show : false
			                    }
			                },
			                emphasis : {
			                    label : {
			                        show : true,
			                        position : 'center',
			                        textStyle : {
			                            fontSize : '30',
			                            fontWeight : 'bold'
			                        }
			                    }
			                }
			            },
			            data:[
			                {value:335, name:'直接访问'},
			                {value:310, name:'邮件营销'},
			                {value:234, name:'联盟广告'},
			                {value:135, name:'视频广告'},
			                {value:1548, name:'搜索引擎'}
			            ]
			        }
			    ]
			};
			                    

		// 为echarts对象加载数据 
		myChart.setOption(option);
	
		function selectAll(checkbox){
			if(checkbox.checked){
				var myChart = echarts.init(document.getElementById('main'));			
				this.option.legend.selected = {'直接访问':true, '邮件营销':true, '联盟广告':true, '视频广告':true, '搜索引擎':true};
			}else{
				var myChart = echarts.init(document.getElementById('main'));
				option.legend.selected = {'直接访问':false, '邮件营销':false, '联盟广告':false, '视频广告':false, '搜索引擎':false};
			}
			myChart.setOption(option);
		};
		
	</script>
</body>
</html>