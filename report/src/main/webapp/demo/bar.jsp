<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String contextPath = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>柱状图</title>
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/js/easyui/themes/icon.css">
<script type="text/javascript" src="<%=contextPath %>/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="<%=contextPath %>/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=contextPath %>/js/echarts/echarts-all.js"></script>
</head>
<body>
	
	<div style="height:400px; width: 800px; border: 1px solid #ccc; padding: 3px;">
		<div align="right" style="line-height:25px;vertical-align:middle; border: 1px solid #ccc;padding: 1px;">
		<input type="checkbox" name="checkAll" onclick="selectAll(this);" checked="checked">全选 &nbsp;&nbsp;
			渠道名称：
			<select class="easyui-select" id="" name="">
				<option>全部</option>
				<option value="11">11</option>
				<option value="22">22</option>
			</select>
			时间段
			<input type="text" class="easyui-text" name="beginDate"/>--<input type="text" class="easyui-text" name="endDate"/>
			<a href="#"><img src="<%=contextPath %>/images/excelicon.jpg" align="middle"></a>
		</div>
		<div id="main" style="height:90%; width: 100%;"></div>
	</div>
	<script type="text/javascript">
		// 基于准备好的dom，初始化echarts图表
		var myChart = echarts.init(document.getElementById('main'));

		var option = {
			    title : {
			        text: '某地区蒸发量和降水量'
			    },
			    tooltip : {
			        trigger: 'axis'
			    },
			    legend: {
			        data:['蒸发量','降水量']
			    },
			    toolbox: {
			        show : true,
			        feature : {
			            magicType : {show: true, type: ['line', 'bar']},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    calculable : true,
			    xAxis : [
			        {
			            type : 'category',
			            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value'
			        }
			    ],
			    series : [
			        {
			            name:'蒸发量',
			            type:'bar',
			            data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
			            markPoint : {
			                data : [
			                    {type : 'max', name: '最大值'},
			                    {type : 'min', name: '最小值'}
			                ]
			            },
			            markLine : {
			                data : [
			                    {type : 'average', name: '平均值'}
			                ]
			            }
			        },
			        {
			            name:'降水量',
			            type:'bar',
			            data:[2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
			            markPoint : {
			                data : [
			                    {name : '年最高', value : 182.2, xAxis: 7, yAxis: 183, symbolSize:18},
			                    {name : '年最低', value : 2.3, xAxis: 11, yAxis: 3}
			                ]
			            },
			            markLine : {
			                data : [
			                    {type : 'average', name : '平均值'}
			                ]
			            }
			        }
			    ]
			};
		// 为echarts对象加载数据 
		myChart.setOption(option);
		
		
		function selectAll(checkbox){
			if(checkbox.checked){
				var myChart = echarts.init(document.getElementById('main'));			
				this.option.legend.selected = {'蒸发量':true, '降水量':true};
			}else{
				var myChart = echarts.init(document.getElementById('main'));
				option.legend.selected = {'蒸发量':false, '降水量':false};
			}
			myChart.setOption(option);
		};
	</script>
</body>
</html>