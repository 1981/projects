package com.xuanxiang.ideal.report.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.HttpVersion;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.HeadMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpMethodParams;


import org.apache.log4j.Logger;

import com.meidusa.fastjson.JSON;

public class HttpClientHandler {

	public static final Logger logger = Logger.getLogger(HttpClientHandler.class);
	
	private static final long PERIOD_TIME = 1000L * 3600;
	private static HttpClientHandler instance = null;

	private HttpClient httpClient = null;
	private long lastUpdateTime = 0;
	private int connectTimeout = 1000;
	private int soTimeout = 2000;
	private int maxConnectionsPerHost = 100;
	private int maxTotalConnections = 1000;

	static {
		instance = newInstance();
	}

	private HttpClientHandler() {
		reinit();
	}

	private HttpClientHandler(int soTimeout) {
		this.soTimeout = soTimeout;
		reinit();
	}

	public static HttpClientHandler newInstance() {
		return new HttpClientHandler();
	}

	public static HttpClientHandler newInstance(int soTimeout) {
		return new HttpClientHandler(soTimeout);
	}

	public void reinit() {
		HttpClientParams params = new HttpClientParams();
		params.setConnectionManagerClass(MultiThreadedHttpConnectionManager.class);
		params.setConnectionManagerTimeout(connectTimeout);
		params.setSoTimeout(soTimeout);
		params.setVersion(HttpVersion.HTTP_1_1);
		httpClient = new HttpClient(params);
		HttpConnectionManagerParams param = new HttpConnectionManagerParams();
		param.setConnectionTimeout(connectTimeout);
		param.setSoTimeout(soTimeout);
		param.setDefaultMaxConnectionsPerHost(maxConnectionsPerHost);
		param.setMaxTotalConnections(maxTotalConnections);
		httpClient.getHttpConnectionManager().setParams(param);
		if (logger.isInfoEnabled()) {
			logger.info("current properties: \"connectTimeout:"
					+ connectTimeout
					+ "\" , soTimeout:"
					+ soTimeout
					+ "\" , maxConnectionsPerHost:"
					+ maxConnectionsPerHost
					+ "\"\n\r"
					+ "if you wanna change the properties and make effective,please call reInit() method after you set new properties!");
		}
	}

	public static void reInit() {
		instance.reinit();
	}

	public static HttpClient getHttpClient() {
		return instance.gethttpclient();
	}

	private HttpClient gethttpclient() {
		if (System.currentTimeMillis() - lastUpdateTime > PERIOD_TIME) {
			checkReleaseConnection();
		}
		httpClient.setState(new HttpState());
		return httpClient;
	}

	private synchronized void checkReleaseConnection() {
		lastUpdateTime = System.currentTimeMillis();
		try {
			httpClient.getHttpConnectionManager().closeIdleConnections(1);
			if (logger.isInfoEnabled()) {
				logger.info("free connections successfully!");
			}
		} catch (Exception e) {
			logger.error("", e);
		}
	}

	public String sendRequest(String httpAddr, boolean isGet, String charset) {
		HttpMethod httpMethod = null;
		try {
			HttpClient client = gethttpclient();
			if (isGet) {
				httpMethod = new GetMethod(httpAddr);
			} else {
				httpMethod = new PostMethod(httpAddr);
			}
			HttpMethodParams arg0 = new HttpMethodParams();
			arg0.setContentCharset(charset);
			arg0.setSoTimeout(client.getParams().getSoTimeout());
			httpMethod.setParams(arg0);
			setHttpHeader(httpMethod);
			client.executeMethod(httpMethod);
			int httpStatus = httpMethod.getStatusCode();
			if (httpStatus == 200 || (httpStatus > 300 && httpStatus < 400)) {
				return httpMethod.getResponseBodyAsString();
			} else {
				logger.error("httpStatus:" + httpMethod.getStatusCode() + ","+ httpAddr);
				throw new IOException("httpStatus:"+ httpMethod.getStatusCode());
			}
		} catch (HttpException e) {
			logger.error("HttpException", e);
			throw new RuntimeException("HttpException", e);
		} catch (IOException e) {
			logger.error("IOException", e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			logger.error("OtherException", e);
			return null;
		} finally {
			if (httpMethod != null){
				httpMethod.releaseConnection();
			}
		}
	}

	public static boolean isExists(String httpAddr) {
		return instance.isexists(httpAddr);
	}

	public boolean isexists(String httpAddr) {
		HttpMethod httpMethod = null;
		try {
			HttpClient client = gethttpclient();
			httpMethod = new HeadMethod(httpAddr);
			HttpMethodParams arg0 = new HttpMethodParams();
			arg0.setSoTimeout(client.getParams().getSoTimeout());
			httpMethod.setParams(arg0);
			setHttpHeader(httpMethod);
			client.executeMethod(httpMethod);
			int httpStatus = httpMethod.getStatusCode();
			if (httpStatus == 200 || (httpStatus > 300 && httpStatus < 400)) {
				return true;
			} else {
				return false;
			}
		} catch (HttpException e) {
			logger.error("HttpException", e);
			throw new RuntimeException("HttpException", e);
		} catch (IOException e) {
			logger.error("IOException", e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			logger.error("OtherException", e);
		} finally {
			if (httpMethod != null)
				httpMethod.releaseConnection();
		}
		return false;
	}

	
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> sendRequest(String httpAddr, boolean isGet,Map<String, Object> params, String charset) {
		logger.info("request for http :" + httpAddr + " isGet :" + isGet + "charrset : " + charset);
		
		logger.info("request params :" + JSON.toJSONString(params));
		
		String result = instance.sendRequest(httpAddr, isGet, params, null, charset) ;
		
		return result == null ? null : (Map<String,Object>) JSON.toJavaObject(JSON.parseObject(result), Map.class);
	}

	public String sendRequest(String httpAddr, boolean isGet,Map<String, Object> params, Map<String, String> headerParams,String charset) {
		
		HttpMethod httpMethod = null;
		try {
			HttpClient client = gethttpclient();
			if (isGet) {
				httpMethod = new GetMethod(httpAddr);
			} else {
				httpMethod = new PostMethod(httpAddr);
			}
			HttpMethodParams arg0 = new HttpMethodParams();
			arg0.setContentCharset(charset);
			arg0.setSoTimeout(client.getParams().getSoTimeout());
			httpMethod.setParams(arg0);
			if (params != null && !isGet) {
				Iterator<String> it = params.keySet().iterator();
				while (it.hasNext()) {
					String name = it.next();
					Object value = params.get(name);
					if (value instanceof List) {
						@SuppressWarnings("rawtypes")
						List list = (List) value;

						for (Object object : list) {
							((PostMethod) httpMethod).addParameter(name,
									String.valueOf(object));
						}
					} else {
						((PostMethod) httpMethod).addParameter(name,
								String.valueOf(value));
					}
				}

			}
			setHttpHeader(httpMethod);
			if (headerParams != null) {
				for (Entry<String, String> e : headerParams.entrySet()) {
					httpMethod.setRequestHeader(e.getKey(), e.getValue());
				}
			}
			getHttpClient().executeMethod(httpMethod);
			int httpStatus = httpMethod.getStatusCode();
			if (httpStatus == 200 || (httpStatus > 300 && httpStatus < 400)) {
				return httpMethod.getResponseBodyAsString();
			} else {
				logger.error("httpStatus:" + httpMethod.getStatusCode() + ","+ httpAddr);
				throw new IOException("httpStatus:"+ httpMethod.getStatusCode());
			}
		} catch (HttpException e) {
			logger.error("HttpException", e);
			throw new RuntimeException(e);
		} catch (IOException e) {
			logger.error("IOException", e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			logger.error("OtherException", e);
			return null;
		} finally {
			httpMethod.releaseConnection();
		}
	}

	private static void setHttpHeader(HttpMethod httpMethod) {
		httpMethod.setRequestHeader("Connection", "keep-alive");
		httpMethod.setRequestHeader("User-Agent","Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.0.12) Gecko/2009070611 Firefox/3.0.12 (.NET CLR3.5.30729)");
		httpMethod.setRequestHeader("refere", "http://www.weicaifu.com/");
	}

	@SuppressWarnings("unused")
	private static String getHostAddr(HttpMethod httpMethod) {
		try {
			return httpMethod.getURI().getHost();
		} catch (Exception e) {
			return "";
		}
	}

	public static void setConnectTimeout(int timeout) {
		instance.setconnecttimeout(timeout);
	}

	public void setconnecttimeout(int timeout) {
		this.connectTimeout = timeout;
	}

	public static void setSoTimeout(int timeout) {
		instance.setsotimeout(timeout);
	}

	public void setsotimeout(int timeout) {
		this.soTimeout = timeout;
	}

	public static void setMaxConnectionsPerHost(int maxConnections) {
		instance.setmaxconnectionsperhost(maxConnections);
	}

	public void setmaxconnectionsperhost(int maxConnections) {
		this.maxConnectionsPerHost = maxConnections;
	}

	public int getMaxTotalConnections() {
		return maxTotalConnections;
	}

	public void setmaxtotalconnections(int maxTotalConnections) {
		this.maxTotalConnections = maxTotalConnections;
	}

	public static void setMaxTotalConnections(int maxConnections) {
		instance.setmaxtotalconnections(maxConnections);
	}

}
