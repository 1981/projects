package com.xuanxiang.ideal.report.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public interface ChannelTrendService {

	public List<String> queryChannelNames(Map<String,Object> params) ;

	public List<String> queryTimesData(Map<String,Object> params) ;
	
	public List<Map<String,Object>> queryData(Map<String,Object> params) ;
	
	
	public HSSFWorkbook exportExcel(Map<String,Object> params)  throws Exception ;
	
	
	
	
}
