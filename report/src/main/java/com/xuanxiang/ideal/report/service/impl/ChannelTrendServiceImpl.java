package com.xuanxiang.ideal.report.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.xuanxiang.ideal.report.dao.ChannelDao;
import com.xuanxiang.ideal.report.po.ChannelExcelBean;
import com.xuanxiang.ideal.report.service.ChannelTrendService;
import com.xuanxiang.ideal.report.util.ExcelUtil;

public class ChannelTrendServiceImpl implements ChannelTrendService {
	
	@Autowired
	ChannelDao ChannelDao ;
	
	
	@Override
	public List<String> queryChannelNames(Map<String, Object> params) {
		return null;
	}

	@Override
	public List<String> queryTimesData(Map<String, Object> params) {
		return null;
	}

	@Override
	public List<Map<String, Object>> queryData(Map<String, Object> params) {
		return null;
	}

	/**
	 * @throws Exception 
	 * 
	 */
	public HSSFWorkbook exportExcel(Map<String, Object> params) throws Exception {
		ExcelUtil<ChannelExcelBean> excelUtil = new ExcelUtil<ChannelExcelBean>(); 
		String title ="渠道接入趋势图";
		
		String[] headers = new String[]{"租户名称","时间","渠道名称","会话量","人工量","自助量","渠道占比(%)"};
		
		List<ChannelExcelBean> dataList = ChannelDao.queryExcelDataList(params);
		
		return excelUtil.exportExcel(title, headers, dataList);
	}

}
