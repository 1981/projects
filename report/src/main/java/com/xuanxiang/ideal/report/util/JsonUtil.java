package com.xuanxiang.ideal.report.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.meidusa.fastjson.JSON;
import com.meidusa.fastjson.JSONArray;
import com.meidusa.fastjson.JSONObject;



public class JsonUtil {

    public static String toJsonString(Object obj){
        String jsonString = JSONObject.toJSONString(obj) ;
        return jsonString ;
    }
    
    
    @SuppressWarnings("unchecked")
	public static Object toBean(String jsonString,@SuppressWarnings("rawtypes") Class classes){
        Object obj = null ;
        if(jsonString!=null&&jsonString.trim().length()>0){
            obj = JSONObject.parseObject(jsonString, classes);
        }
        return obj ;
    }
	
	public static Map<String,Object> toMapFromJson(String requestJson) {
        JSONObject jsonObj = JSON.parseObject(requestJson) ;
        Map<String,Object> resultMap =  jsonObj.getDataMap() ;
        return resultMap ;
    }
	
	
	public static List<Map<String,Object>> toListMap(String jsonStr,String key){
		List<Map<String,Object>> listMap = null;
		JSONObject json =  JSONObject.parseObject(jsonStr);
		
		JSONArray jsonArray = json.getJSONArray(key) ;
		
		if(jsonArray!=null){
			listMap = new ArrayList<Map<String,Object>>() ;
			for(int i=0;i<jsonArray.size();i++){
				JSONObject tempjson = jsonArray.getJSONObject(i) ;
				Map<String,Object> map = tempjson.getDataMap();
				listMap.add(map);
			}			
		}
		return listMap ;
	}
	
}
