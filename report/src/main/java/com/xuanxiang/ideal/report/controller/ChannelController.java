package com.xuanxiang.ideal.report.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/channel")
public class ChannelController {

	
	@RequestMapping("/realtime")
	public String  queryChanel(){
		
		return "channel" ;
	}
	
	
	@RequestMapping("/history")
	public String  queryHistoryChanel(){
		
		return "channel" ;
	}
	
	
	@RequestMapping("/trend")
	public String  queryChannelTrend(Model model){
		
		return "channelTrend" ;
	}
	
	
	
	
}
