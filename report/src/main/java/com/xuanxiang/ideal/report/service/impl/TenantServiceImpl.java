package com.xuanxiang.ideal.report.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.xuanxiang.ideal.report.dao.TenantDao;
import com.xuanxiang.ideal.report.po.TenantInfo;
import com.xuanxiang.ideal.report.service.TenantService;

public class TenantServiceImpl implements TenantService{
	
	@Autowired
	TenantDao tenantDao ;
	
	/**
	 * 查询租户列表
	 * @param params
	 * @return
	 */
	public List<TenantInfo> queryTenantList(Map<String, String> params) {
		return tenantDao.queryTenantList(params);
	}

}
