package com.xuanxiang.ideal.report.po;

public class ChannelSession {

	
	private String channelName;//渠道名称
	
	private String sessionCount;//服务总量
	
	private String artificialSessions;//人工服务量
	
	private String selfSessions ;//自助服务量
	
	private String proportion; //占比

	

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getSessionCount() {
		return sessionCount;
	}

	public void setSessionCount(String sessionCount) {
		this.sessionCount = sessionCount;
	}

	public String getArtificialSessions() {
		return artificialSessions;
	}

	public void setArtificialSessions(String artificialSessions) {
		this.artificialSessions = artificialSessions;
	}

	public String getSelfSessions() {
		return selfSessions;
	}

	public void setSelfSessions(String selfSessions) {
		this.selfSessions = selfSessions;
	}

	public String getProportion() {
		return proportion;
	}

	public void setProportion(String proportion) {
		this.proportion = proportion;
	}
	
}
