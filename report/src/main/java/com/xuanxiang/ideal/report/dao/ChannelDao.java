package com.xuanxiang.ideal.report.dao;

import java.util.List;
import java.util.Map;

import com.xuanxiang.ideal.report.po.ChannelExcelBean;
import com.xuanxiang.ideal.report.po.ChannelInfo;

public interface ChannelDao {

	
	
	/**
	 * 根据租户code获取对应的渠道列表
	 * @param tenantCode
	 * @return
	 */
	public List<ChannelInfo> queryChannelList(Map<String,String> params) ;
	
	
	
	public List<ChannelExcelBean> queryExcelDataList(Map<String,Object> params) ;
	
	
	
}
