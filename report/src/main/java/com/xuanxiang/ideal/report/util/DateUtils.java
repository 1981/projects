package com.xuanxiang.ideal.report.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

	
	public static String getCurrentDateTime(String formatType) {
        SimpleDateFormat dataFormat = new SimpleDateFormat(formatType);
        return dataFormat.format(new Date());
    }
	
	
	public static Date getDateTime(String strDate) throws ParseException {
        SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dataFormat.parse(strDate);
    }
	
	public static Date getDate(String strDate) throws ParseException {
        SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dataFormat.parse(strDate);
    }
	
	public static Date getCurrDateLastTime(String strDate) throws ParseException {
        SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dataFormat.parse(strDate);
        return new Date(date.getTime()+23*60*60*1000+59*60*1000);
    }
	
	
	
}
