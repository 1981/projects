package com.xuanxiang.ideal.report.service;

import java.util.List;
import java.util.Map;

import com.xuanxiang.ideal.report.po.TenantInfo;

public interface TenantService {
	
	/**
	 * 查询租户列表
	 * @param params
	 * @return
	 */
	List<TenantInfo> queryTenantList(Map<String,String> params);
	
}
