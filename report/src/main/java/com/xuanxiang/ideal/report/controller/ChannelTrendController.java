package com.xuanxiang.ideal.report.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xuanxiang.ideal.report.service.ChannelTrendService;
import com.xuanxiang.ideal.report.util.Constant;
import com.xuanxiang.ideal.report.util.DateUtils;

@Controller
@RequestMapping("/channelTrend")
public class ChannelTrendController {
	
	private ChannelTrendService channelTrendService ;

	
	
	public void setChannelTrendService(ChannelTrendService channelTrendService) {
		this.channelTrendService = channelTrendService;
	}

	@RequestMapping("/init")
	public String queryChannelTrend(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		return "channelTrend";
	}

	@RequestMapping("/channels")
	public String queryChanel() {
		return "channel";
	}



	@RequestMapping("/channelNames")
	public void queryTrendChannels(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter write = response.getWriter();
		String channelNames = "";
		write.write(channelNames);
	}

	@RequestMapping("/xaxis")
	public void queryXaxis(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter write = response.getWriter();
		String channelNames = "";
		write.write(channelNames);
	}
	
	@RequestMapping("/xdata")
	public void queryYaxis(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter write = response.getWriter();
		String channelNames = "";
		write.write(channelNames);
	}
	
	
	@RequestMapping("/exportExcel")
	public void exportExcel(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.setContentType("application/vnd.ms-excel;charset=utf-8"); 
        response.setHeader("Content-Disposition", "attachment;filename="+ new String(("channel.xls").getBytes(), "iso-8859-1"));
        
        
		String rptType = request.getParameter("rptType");
		String beginDate = request.getParameter("beginDate");
		String endDate = request.getParameter("endDate");
		
		Map<String,Object> params = new HashMap<String, Object>();
		
		params.put("rptType", rptType);
		params.put("beginDate", DateUtils.getDate(beginDate));
		
		
		if(Constant.RPT_TYPE_HH.equals(rptType)){
			params.put("endDate", DateUtils.getCurrDateLastTime(beginDate));
		}else{
			params.put("endDate", DateUtils.getDate(endDate));
		}
		HSSFWorkbook workbook = channelTrendService.exportExcel(params);
		OutputStream os = response.getOutputStream();
		workbook.write(os);

	}
	

}
