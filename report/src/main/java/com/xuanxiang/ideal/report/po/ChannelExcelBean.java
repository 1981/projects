package com.xuanxiang.ideal.report.po;

public class ChannelExcelBean {

	private String tenantName;
	
	private String rptDate;
	
	private String channelName;
	
	private Integer sessionCount;
	
	private Integer artificialCount;
	
	private Integer selfCount;
	
	private String proportion ;
	
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getRptDate() {
		return rptDate;
	}
	public void setRptDate(String rptDate) {
		this.rptDate = rptDate;
	}
	public Integer getSessionCount() {
		return sessionCount;
	}
	public void setSessionCount(Integer sessionCount) {
		this.sessionCount = sessionCount;
	}
	public Integer getArtificialCount() {
		return artificialCount;
	}
	public void setArtificialCount(Integer artificialCount) {
		this.artificialCount = artificialCount;
	}
	public Integer getSelfCount() {
		return selfCount;
	}
	public void setSelfCount(Integer selfCount) {
		this.selfCount = selfCount;
	}
	public String getProportion() {
		return proportion;
	}
	public void setProportion(String proportion) {
		this.proportion = proportion;
	}
	
	
}
