package com.xuanxiang.ideal.report.dao;

import java.util.List;
import java.util.Map;

import com.xuanxiang.ideal.report.po.TenantInfo;

public interface TenantDao {
	
	/**
	 * 查询租户列表
	 * @param params
	 * @return
	 */
	List<TenantInfo> queryTenantList(Map<String,String> params) ; 
}
