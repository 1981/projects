package com.xuanxiang.ideal.report.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xuanxiang.ideal.report.po.TenantInfo;
import com.xuanxiang.ideal.report.service.TenantService;

@Controller
public class TenantController {

	private TenantService tenantService ;
	
	
	
	public void setTenantService(TenantService tenantService) {
		this.tenantService = tenantService;
	}


	@RequestMapping(value="tenantList")
	public String queryTenantList(HttpServletRequest request,Model model){
		Map<String,String> params = new HashMap<String, String>();
		String tenantName = request.getParameter("tenantName");
		
		params.put("tenantName", tenantName);
		
		List<TenantInfo> tenantList = tenantService.queryTenantList(params);
		
		model.addAttribute("tenants", tenantList);
		
		return "tenant";
	}
	
	
}
