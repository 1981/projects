package com.xuanxiang.ideal.report.po;

/**
 * 渠道基础信息
 * @author qinyajun
 *
 */
public class ChannelInfo {
	
	/**
	 * 渠道ID
	 */
	private Integer autoId;
	
	/**
	 * 渠道code
	 */
	private String channelCode;
	
	/**
	 * 渠道名称
	 */
	private String channelName;
	
	/**
	 * 渠道英文名
	 */
	private String channelEn;
	
	public Integer getAutoId() {
		return autoId;
	}
	public void setAutoId(Integer autoId) {
		this.autoId = autoId;
	}
	public String getChannelCode() {
		return channelCode;
	}
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getChannelEn() {
		return channelEn;
	}
	public void setChannelEn(String channelEn) {
		this.channelEn = channelEn;
	}
	
}
