package com.xuanxiang.ideal.report.po;

import java.util.Map;

public class SeatInfo {

	private String autoId;//	坐席id
	private String deptId	;//组织结构id
	private String workNo	; //工号
	private String userName	;//名称
	private String tenantCode ;//租户code
	private String tel	;//电话
	private String email ;// 	邮箱
	private String teamId;//	班组id
	private String autoBusy	; //自动示忙
	private String autoAnswer ;//自动回复
	private String agentType; //坐席类型
	private String exNo	;//分机号
	private String skillQueue ;//技能组Id
	private String skillQueueName ;//技能组名称
	private String skillTypeCode ;//技能组类型code
	private String skillTypeName ;//	技能组类型名称
	private Map<String,String> skillTypeNum	;// 坐席能处理的技能组类型会话数
	private String status	;//	坐席状态 0 未登录 1 登录未迁入 2签入 3示闲 4 示忙
	private String statusDesc	;//	坐席状态描述
	private String loginTime ;//登录时间
	private String signTime	;//	签入时间
	private String optTime	;//	操作时间
	private String isForce ;//	是否强制 Y强制
	private String channelArr ;//	支持渠道类型
	private String businessArr	;//	支持业务类型
	private String maxSessionCount	;//	最大会话数
	private String currSessionCount	;//	当前会话数
	private String clientType	;//登录客户端类型
	private String signId ;//签入日志id
	private String bfId	;//示忙、示闲日志Id
	private String avayaState ;//avaya状态
	private String location	;//坐席坐标信息
	public String getAutoId() {
		return autoId;
	}
	public void setAutoId(String autoId) {
		this.autoId = autoId;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getWorkNo() {
		return workNo;
	}
	public void setWorkNo(String workNo) {
		this.workNo = workNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getTenantCode() {
		return tenantCode;
	}
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTeamId() {
		return teamId;
	}
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}
	public String getAutoBusy() {
		return autoBusy;
	}
	public void setAutoBusy(String autoBusy) {
		this.autoBusy = autoBusy;
	}
	public String getAutoAnswer() {
		return autoAnswer;
	}
	public void setAutoAnswer(String autoAnswer) {
		this.autoAnswer = autoAnswer;
	}
	public String getAgentType() {
		return agentType;
	}
	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}
	public String getExNo() {
		return exNo;
	}
	public void setExNo(String exNo) {
		this.exNo = exNo;
	}
	public String getSkillQueue() {
		return skillQueue;
	}
	public void setSkillQueue(String skillQueue) {
		this.skillQueue = skillQueue;
	}
	public String getSkillQueueName() {
		return skillQueueName;
	}
	public void setSkillQueueName(String skillQueueName) {
		this.skillQueueName = skillQueueName;
	}
	public String getSkillTypeCode() {
		return skillTypeCode;
	}
	public void setSkillTypeCode(String skillTypeCode) {
		this.skillTypeCode = skillTypeCode;
	}
	public String getSkillTypeName() {
		return skillTypeName;
	}
	public void setSkillTypeName(String skillTypeName) {
		this.skillTypeName = skillTypeName;
	}
	public Map<String, String> getSkillTypeNum() {
		return skillTypeNum;
	}
	public void setSkillTypeNum(Map<String, String> skillTypeNum) {
		this.skillTypeNum = skillTypeNum;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}
	public String getSignTime() {
		return signTime;
	}
	public void setSignTime(String signTime) {
		this.signTime = signTime;
	}
	public String getOptTime() {
		return optTime;
	}
	public void setOptTime(String optTime) {
		this.optTime = optTime;
	}
	public String getIsForce() {
		return isForce;
	}
	public void setIsForce(String isForce) {
		this.isForce = isForce;
	}
	public String getChannelArr() {
		return channelArr;
	}
	public void setChannelArr(String channelArr) {
		this.channelArr = channelArr;
	}
	public String getBusinessArr() {
		return businessArr;
	}
	public void setBusinessArr(String businessArr) {
		this.businessArr = businessArr;
	}
	public String getMaxSessionCount() {
		return maxSessionCount;
	}
	public void setMaxSessionCount(String maxSessionCount) {
		this.maxSessionCount = maxSessionCount;
	}
	public String getCurrSessionCount() {
		return currSessionCount;
	}
	public void setCurrSessionCount(String currSessionCount) {
		this.currSessionCount = currSessionCount;
	}
	public String getClientType() {
		return clientType;
	}
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	public String getSignId() {
		return signId;
	}
	public void setSignId(String signId) {
		this.signId = signId;
	}
	public String getBfId() {
		return bfId;
	}
	public void setBfId(String bfId) {
		this.bfId = bfId;
	}
	public String getAvayaState() {
		return avayaState;
	}
	public void setAvayaState(String avayaState) {
		this.avayaState = avayaState;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	
}
